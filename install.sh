#!/bin/bash
## 
## Made by  armita_a and de-dum_m
## Login   <armita_a@epitech.net> & <de-dum_m@epitech.eu>
## 

FILE=$HOME/.config/epiplan
TEMP=$FILE/temp 
MAINDIR="$HOME/.config/epiplan"
CREDS="$MAINDIR/creds"

function zenity_install()
{
    if [[ `whereis zenity | awk '{print $2}' | grep -v "\n"` ]]
    then
	echo "Zenity is not installed";
	exit;
    fi
}

function download()
{
    echo salut
    
    if [[ "`echo $SHELL`" == "/usr/bin/zsh" ]]
    then
	echo "export PATH=$PATH:$HOME/bin" >> $HOME/.zshrc
    else
	echo "export PATH=$PATH:$HOME/bin" >> $HOME/.basrc
    fi
    export PATH=$PATH:$HOME/bin
}

function cron_creation()
{
    test -d $FILE || mkdir -p $FILE
    crontab -l > $TEMP 2> /dev/null
    cat $TEMP | grep -v epiplan > $TEMP
    echo "@hourly bash $HOME/bin/epiplan_update" >> $TEMP
    echo "*/5 * * * * bash $HOME/bin/epiplan_event" >> $TEMP
    crontab < $TEMP
    rm -rf $TEMP
}

function install()
{
    test -d $HOME/bin || mkdir -p $HOME/bin
    test -d $HOME/epiplan || mkdir -p $HOME/bin/epiplan
    cp epiplan_update $HOME/bin/epiplan_update
    chmod +x $HOME/bin/epiplan_update
    cp epiplan_event $HOME/bin/epiplan_event
    chmod +x $HOME/bin/epiplan_event
    cp token $HOME/bin/token
    chmod +x $HOME/bin/token
}

function configuration()
{
    getlog=`zenity --forms \
--title="Epiplan" \
--text="Identification" \
--add-entry="Login" \
--add-password="Password" \
--separator="|"`

if [ "$?" -eq 1 ]; then
    exit
fi

LOGIN=`echo "$getlog" | cut -d "|" -f1`
echo $LOGIN > $CREDS
PASS=`echo "$getlog" | cut -d "|" -f2`
echo -e $PASS | openssl enc -base64 >> $CREDS
}

function updating()
{
    bash $HOME/bin/epiplan_update
}

(
echo "10" ; zenity_install
echo "# Downloading packages" ; sleep 1
echo "20" ; download
echo "# Crontabs creation" ; sleep 1
echo "40" ; cron_creation;
echo "# Instalation" ; sleep 1
echo "65" ; install
echo "# configuration" ; sleep 1
echo "80" ; configuration
echo "# Starting" ; sleep 1
echo "100" ; updating

) |
zenity --progress --auto-close\
  --title="Instalation of Epiplan" \
  --text="Instalation of Zenity" \
  --percentage=0

if [ "$?" = -1 ] ; then
  zenity --error \
    --text="Installtion aborted"
fi
