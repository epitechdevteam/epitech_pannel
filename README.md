#Planning Epitech
## Ecrite en bash
###Par armita_a et de-dum_m


```
commandes:  
epiplan_update	permet de mettre a jour le planning
epiplan_event	permet de visualiser le prochain event
token		permet de saisir son token  
  
```

---  
  
# Installation  
## Dépendances:   
  - zenity

### root:  
```
sudo zypper install zenity  
    
./install.sh
  
```

---

> Il en manque beaucoup, donc n'hésitez pas a faire des "pull requests" si vous avez quelque chose à ajouter.
> Remontez-nous les faux positifs/négatifs ou autres bugs, ou mieux corrigez les et on mergera.
> 
> Email : <de-dum_m@epitech.eu> ou  <armita_a@epitech.eu>
> 
> Signalement de bugs : [ICI](https://bitbucket.org/epitechdevteam/epitech_pannel/issues?status=new&status=open)
